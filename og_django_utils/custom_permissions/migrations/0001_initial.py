# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-08-17 14:08
from __future__ import unicode_literals

from django.db import migrations, models
import og_django_utils.custom_permissions.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomPermissionModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'verbose_name': 'Uprawnienie systemowe',
                'managed': False,
                'verbose_name_plural': 'Uprawnienia systemowe',
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='CustomPermission',
            fields=[
            ],
            options={
                'default_permissions': (),
                'proxy': True,
            },
            bases=('auth.permission',),
            managers=[
                ('objects', og_django_utils.custom_permissions.models.CustomPermissionManager()),
            ],
        ),
    ]
